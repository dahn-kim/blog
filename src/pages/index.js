import React from "react"
import { Link, graphql } from "gatsby"
import indexStyles from "../styles/index.module.css"


export default function Home({ data }){
  console.log(data)
  return(
     <div className={indexStyles.index}>
        <Link to='/about/' className={indexStyles.about}>About</Link>
        <h1>This is a simple-AF Blog.</h1>  
        {data.allMarkdownRemark.edges.map(({ node }) => (
          <div className={indexStyles.postPreview} key={node.id}>
            <Link className={indexStyles.link} to={node.fields.slug}>
              {node.frontmatter.title}
            </Link>
            <div className={indexStyles.postDate}>{node.frontmatter.date}</div>
            <div className={indexStyles.excerpt}><p>{node.excerpt}</p></div>
          </div>
        ))}
        <footer> © 2020 Daeun Kim <span role="img" aria-label="v">✌️</span> </footer>
     </div>
  )
} 


export const query = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }){
      totalCount
      edges {
        node {
          id
          excerpt
          frontmatter {
            title
            date(formatString: "DD MMMM, YYYY")
          }
          fields {
            slug
          }
          
        }
      }
    }
  }
` 



/*class IndexComponent extends React.Component{
  render(){
    const { data } = this.props
    const title = data.site.siteMetadata.title
    //const description = data.siteMetadata.description
    //const date = data.siteMetadata.date

    return(
     
    )
  }
}


export default IndexComponent*/

/*export default function Home() {
  return (
    <div style={{ color: `purple` }}>
      <Link to='/contact/'>Contact</Link>
      <Link to='/about/'>About</Link>
      <Header headerText="Hello world!" />
    </div>
  );
} */