---
title: "Client-Server web development studies using javascript"
date: "2020-08-07"
---

The entire note is from a post written by Alejhandero Hernandez published on toptal.com
<https://www.toptal.com/javascript/guide-to-full-stack-javascript-initjs>

#### A little bit of important histories of javascript and node.js

- Javascript was actually born with Javascript server side in netscape earlier, but wasnt ready to use.
- Node.js is a js framework which writes custom servers. Custom servers can connect my codes and other stuff like OAuth for Twitter API
- Later on, Node.js → emerged including Javascript on the server, also promoted the idea of non-blocking programming. 👏👏👏👏
- node.js on terminal -> like a real-time console 👏👏👏👏👏
- Node.js allows to use non-blocking programming such as callbacks and an event loop. 

#### Node.js
- Node.js is rather a runtime environment for any scalable network javascript application
- MongoDB is a NoSQL document-based database that uses Javascript as a query language, enabling the end-to-end JavaScript platform. a schema-less database that allows your objects to be in a flexible and to adapt faster to changes. map-reduced based which is suitable for big data applications.

#### NPM 
- npm -> Node Package Manager (ah hA!!!!) also another command to run and execute various projects associated with javascript. npm helps to open and run open sources and packages available online..
- npm's package.json -> "setting", "config", "dependencies"
- creating your own package via 'npm init' which will automatically create a 'package.json' file
- npm website provides packages available online
- npm install 'name of the package' --save (--save functions saves the package in your package.json file under the dependencies)

#### Express js
- Express.js -> when you want to make a quick web server for a project. available on npm open sources.
- Express.js → Server-side componentization. Your component is part of a pipeline. The pipeline processes a request(input) and generates a response (output). In express.js two types of middleware are created.
![express.js scope](https://bs-uploads.toptal.io/blackfish-uploads/uploaded_file/file/194977/image-1582841556191-955ccc9045aa16077a24fb967fba76a6.png)
