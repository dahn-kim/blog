---
title: "Creating a FHIR app using Angular "
date: "2020-10-01"
---
### Introduction
In February 2019, the European Commission published a new recommendation including the improvement of each member state’s electronic health record (EHR) system in an interoperable way. The EU Commission states that interoperable EHR system within the EU network would provide medical data to citizens, healthcare providers and for cross-border exchange within different members states in a more secure way. [1]


Since the SARS-CoV-2, as known as COVID-19 has led the pandemic situation this year, physical mobility has been limited for infection prevention. Therefore, digital way of exchanging COVID-19 related medical data has been important for monitoring and research and development. [2] [3]


There are many countries that patient- and medical data are handled independently by clinics, municipalities or sub nations, whereas a few countries have already implemented nation-wide standardised health information system. Such as in Germany, Austria or South Korea, whenever patient visits different doctors, they have to register herself/himself each time.


Furthermore, for a faster COVID-19 testing process, in laboratories and clinics provide testing service apart from national testing service. Yet, it is unsure how testing result data is exchanged between individual health providers and government institutions such as health authority and municipal department.
Fhir app was built inspired by the motive of exchanging standardised health data for local health providers, municipalities and health authorities as a target user group.

### Application: Fhir app
Fhir app was developed based on an imaginary condition that a national health authority has provided a national/regional server network to exchange health data between health providers.

The application consists of two main features:

1. Display recent COVID-19 testing results in an imaginary local area for health providers and health authority.
2. Creating a simple patient profile in a standardized way and store it in a server.

#### Material and Methods

##### FHIR (Fast Healthcare Interoperable Resources)
FHIR (Fast Healthcare Interoperable Resources) is the new standard to exchange resources in XML and JSON formats. It was developed from Health Level Seven International (HL7) and is based on REST-Architecture. FHIR can be used for example to share data between EHR systems, for the communication between servers of healthcare providers or within mobile applications. [4]

##### HAPI FHIR Public Test Server
To store and retrieve FHIR resources, HAPI FHIR Public test server was used as the project’s data source. The server is a complete implementation of a FHIR server which also stores diverse FHIR resources globally received from http post requests. Therefore, HAPI FHIR public test server was decided to use to retrieve FHIR-standardised artificial COVID-19 test results and to create and store FHIR patient resources. However, since the focus of the project is to exchange interoperable medical data, a secured server network should be used in real time. [5]

##### LOINC (Logical Observation Identifiers Names and Codes)
LOINC provides internationally standardised terminologies to identify health measurements, observations and documents. As FHIR requires standardised terminologies such as the title of diagnosis or methodologies used from a lab tests, COVID-19 terminology was referenced from LOINC. [6]

##### Angular framework & Angular Material UI component library
Angular framework was used to build a web application and to handle the http client side to retrieve COVID-19 test result FHIR resources and to create patient FHIR resource via HAPI FHIR public test server. Angular Material UI component library was used to improve the aesthetics of front-end feature of the application. [7] [8]

##### Gitlab & Microsoft Azure DevOps
To maintain the version control of code in track, Gitlab repository was used. For deployment of Fhir app, App Services and DevOps from Microsoft Azure were used.

### Solution
![figure1](https://gitlab.com/dahn-kim/fhir-app/-/raw/master/readme_fig_1.png)
![figure2](https://gitlab.com/dahn-kim/fhir-app/-/raw/master/readme_fig_2.png)


As seen on the figure 1 and figure 2, for the COVID-19 test result, a local health care provider will see a list of recent test results retrieved from the HAPI FHIR public test server. It is assumed that test result resources are created as a FHIR Observation resource and sent from any local health care providers. The list shows the updated date, document ID, result (positive(detective) / negative(not-detective)) and patient information. Instead of relying on national level of updated COVID-19 cases, this will enable local health providers and municipal authorities monitor the local cases better.


For the second feature of adding new patient, health care providers fill in the patient form in the app in the beginning. Once the ‘submit’ button is clicked, all the attributes are bound to be created as a FHIR patient resource and stored to the HAPI FHIR public test server. The new patient resource can be accessed with the returned URL with the created resource ID.

#### Source code
https://gitlab.com/dahn-kim/fhir-app

#### Application location
https://fhir-app.azurewebsites.net/


### References
[1] E. Comission, "Recommendation on a European Electronic Health Record exchange format," European Comission, 09 02 2019. [Online]. Available: https://ec.europa.eu/digital-single-market/en/news/recommendation-european-electronic-health-record-exchange-format.

[2] U. C. a. L. Governments, "Digital Technologies and the COVID19 pandemic," 15 4 2020. [Online]. Available: https://www.uclg.org/sites/default/files/eng_briefing_technology_final_x.pdf.

[3] U. AID, "COVID-19 AND DIGITAL DATA COLLECTION," 29 7 2020. [Online]. Available: https://www.usaid.gov/digital-development/covid-19/digital-data-collection.

[4] "HL7 FHIR," [Online]. Available: https://www.hl7.org/fhir/.

[5] "HAPI FHIR," [Online]. Available: https://hapifhir.io/.

[6] LOINC, "SARS-CoV-2 and COVID-19 related LOINC terms," LOINC, [Online]. Available: https://loinc.org/sars-cov-2-and-covid-19/.


[7] "Angular," [Online]. Available: https://angular.io/.

[8] "Angular Material," [Online]. Available: https://material.angular.io/.

