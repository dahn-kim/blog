---
title: "Framework vs Library and other web development studies"
date: "2020-09-09"
---

#### Recap of node.js

node.js is a javascript framework for web development and applications. javascript, the programming language which was built to use for web browsers. this node.js framework enables javascript to run locally in developer's computer machine, which also means we can build mobile applications and learn server programming like me! Therefore, express.js is also a node.js web application framework. ✌️

#### Framework vs Library

Library provides additional functionalities and developers can import and use specific functions in their code. Developer has the control of the whole code including the use of library. e.g. various python libraries for data sciences (numpy, pandas, matplotlib.) 

While framework provides the rules that developers have to follow accordingly. Programming within a framework is controlled by the framework, not the developer. e.g. node.js

#### API

An application programming interface is a computing interface so codes can communicate to each other, such as Restful API.

#### Static web (정적웹) vs Dynamic web (동적웹)

a Fixed web page such as HTML, a company intro page, a blog site without comment function. Like this blog is maintained and updated manually by me, adding or changing the code and pushing to gitlab manually then changes. 

In contrast, a dynamic web provides user interaction and changes content frequently like Facebook, twitter and instagram. now I am realising that a UI tool such as react component is a great source for dynamic web/model view controller. Once a user clicks 'submit' to upload their comment to their friend's post, updating the comment section will be give a comfortable look to users and will load faster than loading the whole website again. 🤔🤔
