---
title: "Understanding Asynchronous JavaScript- async/await"
date: "2020-09-10"
---

Asynchronous makes the process more efficient in time wise and enables multitasking. Such as while you are boiling pasta, you can wash vegetables and slicing them. Then when pasta is cooked, bring the pasta and vegetables together and cook them together.

declaring a function as asynchronous indicating like this.;

```javascript
const fetchData = async (url) => {
    try{
        const response = await fetch(url);
        const data = await response.json();
        return data;
    }
    catch(error){
    }
}

fetchData(“https://www.helloworld.com");
```

Writing ‘async’ next to the function parameter will indicate the function is asynchronous. Therefore, the function is also able to use await keyword inside the function.

whenever this occurs, js pauses the function until the asynchronous call has been resolved. 


Inside fetchData asynchronous function:
- Begin with the fetch call with the url
- wait until the response is receive and store it to the variable
- then continues
- json, and lastly return the data we received and stored as json.


#### Javascript Errors (try and catch)

The try lets you test a block of code for errors.
The catch lets you handle the error.
throw lets you create custom error
finally lets you execute code, after try and catch, regardless of the result.

```javascript
try {
    nonExistingFunction()
} catch(error){
    console.log(error.name + “:” + error.message);
}
fetchData(“https://www.helloworld.com");
```

###### Output

```javascript
ReferenceError:nonExistingFunction is not defined
```
In the catch block, we showed the name and message properties of the error object. The name of the error is ReferenceError and its message is nonExistingFunction is not defined.

try and catch is useful for async and await -> There might be the case that the URL we want to request does not exist. In such cases where we can not successfully resolve an asynchronous call, the further execution would fail. Therefore, we can specify in a separate catch block, how we want to treat such errors (like displaying an error message to the user). So if any of our await statements in the try block fails, the execution jumps right into the catch block.

#### Promises

another way of handling asynchronous code in js. a promise is an object producing/returning a value in the future depending on whether the asynchronous call will resolve well or not.

async/await results a Promise. 

```javascript
const fetchData = async(url) => {
    try { 
    const response = await fetch(url);
    const data = await response.json();
    return data;
    } catch(error){
    }
}

fetchData(“https://www.helloworld.com”)
    .then( json => console.log(json) );
```

Using .then method for Promise. In the example, the fetchData function in line 11 returns a Promise instead of the actual data we want to have. To retrieve the data, we have to wait until the Promise has been resolved (meaning the response of our asynchronous fetchData function has been returned). We can do that by using the then method. This function takes another function as an argument where we pass in the response. In the function body, we can then work with the actual received data of our fetchData call.



**Reference**: https://medium.com/javascript-in-plain-english/async-await-for-beginners-understanding-asynchronous-code-in-javascript-748b57ae94e2