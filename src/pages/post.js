import React from "react"
import { Link } from "gatsby"
import styles from "../styles/post.module.css"
import Footer from "../components/footer"

class IndexComponent extends React.Component{
    render(){
        return(
            <div className={styles.page}>
                <Link className={styles.back} to="/">back to overview →</Link>
                <h2 className={styles.title}>
                Embedding visualisation to HTML
                </h2>
                <div className={styles.date}>published on 16.07.2020</div>    
                <div className={styles.text}>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <p>Sollicitudin aliquam ultrices sagittis orci a scelerisque purus semper. Vitae auctor eu augue ut lectus arcu bibendum. Scelerisque in dictum non consectetur a erat nam at. Urna nec tincidunt praesent semper feugiat nibh sed pulvinar proin. Venenatis lectus magna fringilla urna porttitor rhoncus dolor purus non. Sed libero enim sed faucibus turpis. Feugiat vivamus at augue eget arcu dictum varius duis at. Lectus proin nibh nisl condimentum id venenatis. Purus non enim praesent elementum facilisis leo. Diam vulputate ut pharetra sit amet aliquam id diam maecenas. Dignissim convallis aenean et tortor.</p>
                </div>
                <Footer></Footer>
            </div>
        )
    }
}

export default IndexComponent