import React from "react"
import { Link } from "gatsby"
import indexStyles from "../styles/index.module.css"
import picture from "../images/image1.png"

export default function About(){
    return(
        <div className={indexStyles.index}>
            <Link to="/" className={indexStyles.about}>back to overview →</Link>
            <h2 className={indexStyles.title}>Welcome.</h2>
            <div className={indexStyles.excerpt}>
            <p>You have a longer path figuring out on which pokemon you decide to aim your ball at. </p>
            <p> Previously as a generalist who is interested in doing everything from scrach like filming, editing, 
            project managing, a little bit of front-end and print("heallo world") in Python, designing and branding. It wasn't enough. I looked for where and with I can find my foundation and grow forever?</p> 
            <p>From Media Engineering to Medical IT. I went back to study again because I strived for a new challenge. </p>
            <p> While I wanted to be an engineer from product manager, healtcare IT sounded like the field I can grow my tecnical skills and provide better service to the society. </p>
            <p>Long intro anyways. I built this blog to note down the new things I've learnt from tech stuff. Currently I am interested in web dev+ medical IT / telemedicine / software developent.</p>
            <img src={ picture } alt="fireSpot"/>
            
            <p>image from <Link to="https://www.instagram.com/p/CC8x7UdjeVb/">@waterpokemon</Link> on instagram</p>
            <p>Check out my gitlab profile
                <Link to="https://gitlab.com/dahn-kim">
                <span role="img" aria-label="point right"> 👉</span>my gitlab</Link> 
                <span role="img" aria-label="point left">👈</span>
            </p>
            </div>
            <footer> © 2020 Daeun Kim <span role="img" aria-label="v">✌️</span> </footer>
        </div>
    )
}