---
title: "Biosignal processing"
date: "2021-02-13"
---

Biosignal processing was one of my favourite courses in the winter semester. By practically handling the data extracted during the photoplethysmography(PPG), it was very interesting to learn how modern wellness poratable devices such as smartwatches and rings work. 
### Digital Signal Processing
The series of processes carried out on already digitised signed with measuring, filtering, reproducing analogue signals.
- Signal: Any physical quantity varying one or more independent variables such as time and space. Recordings of continuos time or analogue signals, which has to be first sampled in time into a discremte-time signal. 
- System: A process whose input & output are signals. The class of linear time-invariant systems.
- The most important signal expansions are provided by Fourier transform. 

#### Sampling
- The conversion of continuou and time domain, real-life signals to discrete numbers. Yes we work with the sampled signals!
- The relationship between sampling interval & sampleing rate/frequency are inverse to each other. 
- T(interval) = 0.5sec 👉  F(frequency) = 1/T = 1/0.5sec = 2 samples/second   
- Samples are taken at different intervals, data manipulated mathematically in two domains(time & frequency)

#### Frequency Domain
A transform from time domain to frequency domain. Fourier transformation performed to obtain fourier coefficient. 
- Fourier coefficient: The result of FT, converts the signal information to its phase and magnitude in frequency domain.
- Frequency domain presents easier analysis of the signal spectrum, checking present of absent status of the input signal.

#### Time Domain
Input signal enhancement as a main focus. Filters ussed by passing the surrounding samples through a series of linear transformations.
- Filters: Linear filters, casual filters, time invariant, FIR and Infinite impulse reponse filters.

#### Fast Fourier Transform
- Signal processing (Convolution, digital filters)
- Fast multiplication of large integers
- Solving partial differential equations
- Magnetic resonance imaging used

### Summary: Assignment recap based on G.Papini's PPG beat detection method [1]

Papini's[1] paper introduces an algorithm whic calculates the pulse quality by comparing each individual PPG pulse to a template obtained from a set of pulses via DTW-based averaging. 
    ![Papini](https://gitlab.com/dahn-kim/blog/-/raw/master/src/images/papini-algorithm.png)

#### 1. Pre-Processing
Using two frequencies with two butterworth bandpass filter: PPG 2.25Hz & PPG 10Hz. PPG 2.25Hz for removing high-frequency related noise, the diastolic peak, low frequency related respiration and body position changes. PPG 10Hz for preserving the morphology of PPG pulses in need of exhibiting related to diastolic peak.

#### 2. Segmentation & localisation
- PPG 10Hz is segmented using the local minima of PPG 2.25Hz
- Exclude 1.15s above or 0.5 below duration and outside of 40-120bpm.
- Remaining: unsample to 1kHz with a cubic spline interpolation

#### 3. Pulse Normalisation
Recording may make pulse amplitudes vary, therefore normalisation is necessary in prior to template creation. 
    
PP_Temp = (PP10Hz-shift)/amplitude & PP _PQI = PP10Hz - shift
    
The template derived from the PPs_Temp & each PP _PQI represent temp as a normalised amplitude and PP _PQI as a varying amplitude. 
    
Further steps: 
1. Store a time series, comprising all the amplitude
2. Remove all elements that have a value above and below 50% than the previous or the following value from the amplitude time series.
3. With cubic-spline-interpolation & 4 BW filter, interpolate the clipped amplitude time series at 4 Hz.
4. Resample(replace) the filtered signal at the same time location as the original amplitude time series.
5. The cleaned amplitude time series is used as a series of collection factors.
6. Adjust the template amplitude with the correction factor.
7. Compare the template and PP _PQI.
8. Multiply the template by correction factors.

#### 4. Template creation
Iteration of decreasing the DTW distance between an initial template and each individual pulse. 1) The DBA is initialised with the medoid of the PpsTEMP as initial template. he resulting pulse template is filtered using a 3rd-order lowpass butterworth filter to remove high frequency by DBA.

PpTemp * DBA & TempAd = Correction factor x Template

Calculating PpPQI & TempAd = PpWarped -> DTW with condition

- Dynamic Time Warping(DTW):
    - Measuring the similarity between two time series
    - Originially designed to treat automatic speach recognition
    - Optimal global alignment between two time series, exploiting temporal distortions between them

- Dynamic Time Warping Barycenter Averaging(DBA):
    - An averaging method which consists in iteratively refining an initially average sequence
    - To minimise its squared distance(DTW) to averaged sequences
    - To minimise the sum of squared DTW distances from the average sequence to the set of sequences
    - The sum is formed by single distances between each coordinate of the average sequence and coordinates of sequences associated to it.

- Time-series data:
    - Appylying clustering algorithm
    - Better with dynamic time warping than Euclead distance metric
    - K-medoids algorithm used
    - DBA to improve pairwise averaging

#### PpPQI Adaptation
1. Use DTW to derive PpWared from the warping of PpPQI to the template.

    PpWarped = DTW(PpPQI, Temp)
2. Apply two contrasints to the DTW to avoid distortedly warped PpsPQI.
    - One constraint limits the maximum time warping to 0.3s and the other limits the number of matching points to a maximum of 3.
    - First constraint is derived and corresponds approximately to two times the standard deviation of inter-pulse intervals.
    - The second constraint is chosen to transform a PpPQI from 0.5s to 1.5s and vice versa.

#### PQI Calculation
1. These residual differences between PpPQI and TempAd in PpsWarped are used to calculate the quality index of each PPQ pulse. Search for pulse-temp dissimilarities.
2. RMS of the unmatched points, quantification of the dissimilarity as an error.
3. TempAd amplitude normalisation + correction of the error. 


#### References

[1] Gabriel Papini, et al. : Sinus or not: a new beat detection algorithm based on a pulse morphology quality index to extract normal sinus rhythm beats from wrist-worn photoplethysmography recordings