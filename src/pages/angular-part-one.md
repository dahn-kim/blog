---
title: "Angular app project #1"
date: "2020-09-18"
---

**Reference**: https://angular.io/tutorial/


##### Angular Components

```
ng generate component heroes
```

if this line doesn’t work due to having other module set 

```
ng generate component heroes --module=app.module
```

This commands scaffolds these:
- Creates a directory ’src/app/hero-detail'


Inside the directory these files are generated:
- A CSS file for the component styles
- An HTML file for the component template
- A TypeScript file with a component class named HeroDetailComponent
- A test file for the HeroDetailComponent class (hero-detail.component.spec.ts)
- Furthermore, the command also adds the component as a declaration in the @NgModule decorator in app.module.ts in the app folder.


##### Services

Components shouldn’t fetch or save data directly and they shouldn’t present fake data. Their focus is to present data and delegate data access to a service.


A service will be used to get objects. Angular dependency injection will be introduced to inject it into a Component constructor.

Services  are a great way to share information among classes that don’t know each other. You will create a MessageService and inject it in two places.

1. Inject in HeroService, which uses the service to send a message.
2. Inject in MessageComponent, which displays that message, and also displays in the ID when the user clicks a hero.


##### @Injectable() services

We see in the hero.service.ts -> Injectable symbol is imported from @angular/core into the code and annotates the class with the @Injectable() decorator. **This marks the class as one that participates in the dependency injection system. **
The Service class is going to provide an injectable service and it can also have its now injected dependencies. 

The @Injectable() decorator accepts a metadata object for the service, the same way the @Component decorator did for component classes.

##### Getting the hero data 

The service could get hero data from anywhere -a web service, local storage, or a mock data source.
Removing data access from components means you can change your mind about the implementation anytime, without touching any components.


When you provide the service at the root level, Angular creates a single, shared instance of HeroService and injects into any class that asks for it. Registering the provider in the @Injectable metadata also allows Angular to optimise an app by removing the service if it turns out not to be used after all. 

##### Observable data

Currently getHeroService is written synchronously because we are just mocking, however in real application, it won’t work synchronously. The Service must wait for the server to respond, getHeroes() cannot return immediately with hero data, and the browser will not block while the service waits.


Therefore, HeroService.getHeroes() must have an asynchronous signature. In this tutorial, it will return an Observable because it will eventually use the Angular HttpClient.get method to fetch the heroes and HttpClient.get() returns an Observable. 


of(HEROES) returns an Observable<Hero[]> that emits a single Value, the array of mock heroes.
In the HTTP tutorial, you will call HttpClient.get<Hero[]>() which also returns an Observable<Hero[]> that emits a single value, an array of heroes from the body of the HTTP response.


Observable.subscribe() is the critical difference.
the previous version assigns an array of heroes to the component’s heroes property. The assignment occurs synchronously, as if the server could return heroes instantly or the browser could freeze the UI while it waited for the server’s response.


That won’t work when the HeroService is actually making requests of a remote server.


The new version waits for the Observable to emit the array of heroes - which could happen now or several minutes from now. The subscribe() method passes the emitted array to the callback, which sets the component’s heroes property.


The asynchronous approach will work when the HeroService requests heroes from the server.
