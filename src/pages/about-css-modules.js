import React from "react"
import styles from "./about-css-modules.module.css"


console.log(styles)

const User = props => (
    <div className={styles.user}>
        <img src={props.avatar} className={styles.avatar} alt=""/>
        <div className={styles.description}>
            <h2 className={styles.username}>{props.username}</h2>
            <p className={styles.excerpt}>{props.excerpt}</p>
        </div>
    </div>
)

export default function About(){
    return(
        <div>
            <h1>About CSS Moduels</h1>
            <p>Css Modules are cool</p>
            <User
                username="Jane Doe"
                avatar ="https://s3.amazonaws.com/uifaces/faces/twitter/adellecharles/128.jpg"
                excerpt = "Hello. nice to meet you!"
            />
            <User
                username = "Dahn"
                avatar = "https://s3.amazonaws.com/uifaces/faces/twitter/vladarbatov/128.jpg"
                excerpt = "my first gatsby blog!"
            />
        </div>
    )
}