---
title: "Javascript ES6 - ready for arrow syntax?"
date: "2020-05-08"
---

Since one of the uni projects I have participated required typescript, which means basic knowledge and adequate skills of javascript including es6 style..right? Thank god, typescript is like the mother version of javascript (Object oriented programming language, modules, interface, classes, yay!), I found the language easier and faster to grasp and learn as a python & java person.

But still, understanding the JS ES6 syntax seemed important while using typescript and react for that project. Here are some notes I made and practiced to familiarise with arrow function syntax.

The usual es5 function syntax I am familiar with:
```javascript
  function sum(a,b){
    return a + b
  }
```

Arrow syntax
```javascript
  let sum = (a,b) => {
    return a + b
  }
```
<code> =></code> tells it is a function


Another variation of es6
```javascript
  let sum = (a,b) => a + b
```

es5
```javascript
  function isPositive(number){
    return number >= 0
  }
```

es6: <code>{ }</code> and <code>return</code> were replaced with <code>=></code>.
```javascript
  let isPositive = (number) => number >= 0
```

how far can I go? okay I want to be even more detailed:
```javascript
  let isPositive = number => number >=0
```
Yas, you don't need brackets around your parameter!


Another function to have a look at: ES5
```javascript
  function randomNumber(){
    return Math.random
  }
```

es6
```javascript
  let randomNumber = () => Math.random
```

function syntax with eventListener
```javascript
  document.addEventListener('click', function(){
    console.log('Click')
  })
```


arrow syntax with eventListener
```javascript
  document.addEventListner('click',() => console.log('Click'))
```

es6 arrow syntax in Class
```javascript
  class Student{
    constructor(name){
      this.name = name
    }

    // arrow syntax
    printNameArrow() {
      setTimeout(() => {
        console.log('Arrow: ' + this.name)
      }, 100)
    }

    // function syntax
    printNameFunction(){
      setTimeout(function(){
        console.log('Function: ' + this.name)
      }, 100)
    }
  }
```
