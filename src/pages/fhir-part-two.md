---
title: "HL7 FHIR References & Bundles "
date: "2020-05-21"
---


### Patient with References

Patient with References tutorial

This tutorial will build on the foundation laid out in the simple patient tutorial to introduce resource linking in FHIR by the way of references


1. Setup
    1. Post a Patient resource
2. Find your paitent
    1. https://stu3.test.pyrohealth.net/fhir/Patient/7e42fe1a-c85a-42b3-bdbf-d38b6eebe95a
3. Observe external references
    1. a Patient will not be linked to any of Observation resources, **but all Observation resources will be linked to the Patient.** : No need to keep update Patient resource with the new references. Also means the Patient doesn’t have an explicit link to any of the Observations.
    2. In order to see **which resource links to which**, open up the FHIR specification for a specific resource and observe the list of resources that reference it.
        1. Patient specification

            ![fhir patient specification](https://gitlab.com/dahn-kim/blog/-/raw/master/src/images/fhir-patient-specification.png)
        
        2. Observation specification
        ![fhir observation specification](https://gitlab.com/dahn-kim/blog/-/raw/master/src/images/fhir-observation-specification.png)

    3. on references.xml, one available reference element is “managingOrganization"
        ![fhir managing organisation xml](https://gitlab.com/dahn-kim/blog/-/raw/master/src/images/fhir-managingOrganization-xml.png)
4. Find a patient in Encounter resource with patient reference(id, search parameter)
    ![fhir encounter resource](https://gitlab.com/dahn-kim/blog/-/raw/master/src/images/fhir-Encounter-resource.png)
    1. **https://stu3.test.pyrohealth.net/fhir/Encounter?patient=Patient/<patient id>**
    2. https://stu3.test.pyrohealth.net/fhir/Encounter?patient=Patient/7e42fe1a-c85a-42b3-bdbf-d38b6eebe95a
    3. Once we get our xml back, we notice interesting points:
        1. We get a Bundle resource back
        2. the bundle resource has one result in it
        3. the expected encounter resource is shown, given our Patient has only one Encounter record.

5. Observe contained resource references
    1. References don’t always have to point to a resource located elsewhere, they can also point to resources that are contained(embedded/not having an independent existence, not independently identifiable) in the resource.
      ![fhir resource references](https://gitlab.com/dahn-kim/blog/-/raw/master/src/images/fhir-resource-references.png)

    whenever we see an identifier start with the hash, you will see a corresponding resource within the <contained> field, embedded in the patient resource. such embedding should only be done in the cases where the embedded resource can’t exist on its own - it can’t have an identifier to uniquely identify it or it only makes sense in a particular transaction scope.


### Bundle

Bundle is a container for resources, enabling you to group and transmit resources altogether at once. Guidance on the resource in general is available on FHIR Bundle. and on the RESTful API pages of the FHIR specification, with specific bundle types like transactions, messaging and documents having their own documentation. 

1. About Bundle
    1. Difference between resourced contained in other resources and Bundle.
        1. A Bundle is a collection of resources that have an independent existence - e.g. they can be access directly using the RESTful API
        2. Contained resources cannot be operated by HTTP verbs (GET, PUT, DELETE) outside the containing(parent) resource. 
        3. Resources inside a Bundle, depending on your Bundle type and how you’ve uploaded it. do get created individually on the server and thus can be acted on individually.

2. Bundle Structure
    ![fhir bundle structure](https://gitlab.com/dahn-kim/blog/-/raw/master/src/images/bundle_structure.png)
    
    1. Metadata about the resource: Metadata describing the Bundle type, the total amount of entries (some which may not be visible because of paging), and a link if you need to retrieve this Bundle later.
    2. Main content -> Many entries with 1 resource each/ one entry per resource: The meat of the resource. This is where all resources that the Bundle owns, are stored. Included with resources are URLs which you can use to retrieve the resource individually.
    3. Used in uploading a transaction/bundle: Used when you’re uploading Bundles - this is where you specify to the server what to do with each resource.
    4. Used in downloading a transaction/bundle: Used by the server when it’s responding to your upload request - this is where it says how each operation went.

3. Bundle Types
    1. Searchset: used in searching -> it’s the Bundle that collates search results into a single response. For a larger number return of results, paging is useful.
    2. Document, message, collection: Used in specific data exchange paradigms. document-type Bundles as like a CDA document, a message-type Bundle representing a V2 message and a collection as a general catch-all type.
    3. History: Similar to searchset, but is specialised for the _history operations when you’re looking at the history of operations on single instance of a resource. e.g. looking at all the edits made to a single Patient resource.
    4. Batch, Transaction: Allows you to condense multiple operations into one; e.g. uploading 5 resources at ONCE instead of repeating 5 times. Gather all of the resources up into a bundle and upload it all at once. Difference between a batch and a transaction -> Entire transaction operations fail when one operation fails, while the rest of batch operations continue even when batch operation fails. 
    5. Batch-Response, Transaction-Response: What the server will respond with where it tells you what happened with each operation within your batch or transaction Bundle.

4. Upload endpoints
    1. If uploading a Bundle to [Service Root URL]/Bundle, the Bundle will be stored as it is, like other resources. Sometimes developers use Bundle so the server can process the Bundle. 
        1. Transactions in your transaction Bundle to be acted upon.
        2. In order to get the server to unpack and process your Bundle, you need to upload the [service root URL] directly. This most reliably works for transaction, batch and document Bundle types. How the server deals with other Bundle types is up to it. 
    2. Batch, transaction - endpoint: Bundle is processed by the server-individual operations/resources are unpacked and acted upon.

5. Reference resolution

References between resources within a Bundle can come in two types. Either references to resources where each resource is within the Bundle but not as yet on the server.