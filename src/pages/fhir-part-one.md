---
title: "HL7 FHIR resource "
date: "2020-05-20"
---
### FHIR Patient Resource

#### FHIR Basic tutorial recap
- CRUD operations (Create, Read, Update, Delete) will be used to interact with the FHIR resources with a FHIR server
- REST recap -> Representational state transfer
- Postman using HTTP
    - converting users’ clicks and texts on browser into HTTP to send to the server
    - the server returns content to web page with HTTP
    - For FHIR we will be using XML or JSON instead of a web page, via HTTP client for display.

#### Patient Resource management
1. **GET** your first resource from a FHIR server
    1. we need to post a FHIR(Patient) resource to our FHIR server (tutorial helps to auto-generate a patient resource by clicking a button)

    
    https://stu3.test.pyrohealth.net/
    Server URL


    https://stu3.test.pyrohealth.net/fhir/Patient/124abd
    Server URL
    *‘/fhir/* -> standard
    */Patient/* -> FHIR Resource: Patient
    */124abd/* -> Resource ID
    **we are using REST here** 🙂 


    2. Tell the server what format we want to be returned. We pass it the **‘Accept’** property and it needs to be equal to either **‘application/fhir+xml’** OR **‘application/fhir+json'**


    **KEY: Accept | VALUE: application/fhir+xml or application/fhir+json**
    Configuration in our HTTP post client Postman

2. **PUT** your existing (Patient or any FHIR resources) resource to modify information (name/DOB etc)
    1. Literally means, sending content to the server by way of our Patient resource
    2. **Content-type** property and **application/fhir+xml** or **application/fhir+json**
    
    **KEY: Content-type | VALUE: application/fhir+xml or application/fhir+json**

    3. You can observe updated version ID once the resource is updated.

3. **POST** to create a new Patient Resource
    1. Add **fhir+xml** or **fhir+json** content to body (tab)
    2. for URL we add upto **/Patient** (service root URL/Patient)
    3. if copied from existing resource content, remove the whole id element; otherwise the server will return a 400 Bad Request error.
        1. By posting with the id element removed, the entire resource will be returned with a status of 201 created. New ID will be created. versionID will be set as 1, as the very first version of the resource.

4. **DELETE** to remove an existing Resource
    1. Using the URL below, setting the HTTP verb to **DELETE** and execute
[Service root URL]/Patient/abs1223
    2. **‘204 No Conent’** should return which indicates the resource was successfully removed
    3. Try to retrieve the same resource back again using **_history/[version number you require/ versionId=1]** using **GET**

5. Version ID operation
    1. **Patient** resource, added via a **POST**{this is versionId = 1}
    2. **Patient** resource, updated via a **PUT** {versionId = 2}
    3. **Patient** resource, deleted via a **DELETE** {version Id = 3}
    4. **Patient** resource, attempted to retrieve by resource **id** via a **GET** {no resource will be found}
    5. **Patient** resource, attempted to retrieve by resource **id** and **_history/2** via a **GET** {resource will be found as updated in step 2}
    6. **Patient** source, as retrieve in step 5, added via a **POST** {versionId = 4}

    -- deleting a resource is considered a version update

### FHIR Resourch Search

Simple way of searching patient using names:
* Via using Postman app,
* add the **[service root URL]**, followed by the resource type **Patient**, with **‘?’**, and the search parameters we wish to research **‘family=Fhirman'**
* with the HTTP verb **‘GET’** with **Accept** key property & **application/fhir+xml(json)** as value on **headers (not on parameters!)**
* https://stu3.test.pyrohealth.net/fhir/Patient?family=fhirman
* bundle resource (collection of resources) will be returned. When we search, we will probably receive many resources matching to my search. in FHIR, search results are always returned as a **Bundle. A Bundle is used in FHIR to gather a collection of resource into a single instance.** 
*<Bundle></Bundle> <Patient></Patient>* -> you can spot the difference on xml

Extended Search
* The search parameters (e.g. family) are **Resource type dependent**. Every resource type defines the parameters you can search and offers different dependencies.
* Search parameters are provided on each resource page on FHIR http://hl7.org/fhir/STU3/patient.html


Search Parameter Types
Search data types are categorised into:
* Number
* Date/DateTime
* String
* Token
* Reference
* Composite
* Quantity
* URI
Each and **every search parameter on each resource page** also defines the search **parameter data type** it uses.

*given* -> **string** : A portion of the given name of the patient -> ___Patient.name.given___
*identifier* -> **token** : A patient identifier -> ___Patient.identifier___

**Given** uses **string**(data type) and **identifier** uses **token**(data type)

##### Search Recap
* What datatype does the search parameter **length** use on the **Encounter** resource?
    * -> Quantity
* Using the references above try and find the patient with an **old** address in the suburb of **'Erinsborough'**?
    * -> [service root url]/Patient?address-city=Erinsborough <value use= ‘old />
* Try and find the patient name for the person with an IHI (Individual Health Identifier) of **'8003601043886154'**?
    * ->[service root url]/Patient?identifier=8003601043886154

