import React from "react"
import { graphql, Link } from "gatsby"
import styles from "../styles/post.module.css"


export default function BlogPost({ data }){
    const post = data.markdownRemark

    return(
        <div className={styles.page}>
             <Link className={styles.back} to="/">back to overview →</Link>
             <h2 className={styles.title}>{post.frontmatter.title}</h2>
            <div className={styles.text} dangerouslySetInnerHTML={{ __html: post.html }} />
            <footer> © 2020 Daeun Kim <span role="img" aria-label="v">✌️</span> </footer>
        </div>
    )
}

export const query = graphql`
  query($slug: String!) {
    markdownRemark (fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
          
      }
    }
  }
`