const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)

exports.onCreateNode = ({ node, getNode, actions }) => { //getNode() helper to get file info
    const { createNodeField } = actions
    if (node.internal.type === `MarkdownRemark`){
        const slug = createFilePath({ node, getNode, basePath: `pages`})
        createNodeField({
            node,
            name: `slug`,
            value: slug,
        })
    }
}

//creating pages createPages is an API which Gatsby calls to add pages.

exports.createPages = async ({ graphql, actions }) => {
// **Note:** The graphql function call returns a Promise
// see: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise for more info
    const { createPage } = actions
    const result = await graphql(`
        query {
        allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
            edges {
            node {
                frontmatter {
                    title
                }
                fields {
                slug
                }
            }
            }
        }
    }
    `)
    result.data.allMarkdownRemark.edges.forEach(({ node }) => {
        createPage({
            path: node.fields.slug,
            component: path.resolve(`./src/templates/blog-post.js`),
            context: {
                slug: node.fields.slug,
            }
        })
    })
    console.log(JSON.stringify(result,null,4))
}